import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';

const Transaksi = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 3,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={20} />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 17,
            color: '#034262',
            fontWeight: 'bold',
          }}>
          Transaksi
        </Text>
      </View>
      <TouchableOpacity onPress={() => navigation.navigate('Transaksi2')}>
        <View
          style={{
            width: 350,
            height: 150,
            marginHorizontal: 5,
            marginTop: 25,
            borderRadius: 9,
            justifyContent: 'space-between',
            alignItems: 'center',
            left: 15,
            flexDirection: 'row',
            padding: 19,
            elevation: 3,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 15,
              fontStyle: 'normal',
              textAlign: 'left',
              top: -40,
              left: 2,
              color: 'gray',
            }}>
            20 Desember 2020
          </Text>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 15,
              fontStyle: 'normal',
              textAlign: 'left',
              top: -40,
              left: -180,
              color: 'gray',
            }}>
            09:00
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              left: -680,
              top: -5,
              fontWeight: 'bold',
              color: '#201f26',
              letterSpacing: 1,
            }}>
            New Balance - Pink Abu - 40
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              left: -910,
              top: 16,
              color: 'gray',
              letterSpacing: 1,
            }}>
            Cuci Sepatu
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              left: -1139,
              top: 51,
              color: 'gray',
            }}>
            Kode Reservasi :
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              left: -1250,
              top: 51,
              color: 'gray',
              fontWeight: 'bold',
            }}>
            CS201201
          </Text>
        </View>
        <View
          style={{
            width: 100,
            height: 25,
            left: 260,
            borderRadius: 10,
            backgroundColor: '#F29C1F29',
            opacity: 0.4,
            justifyContent: 'center',
            alignItems: 'center',
            top: -39,
          }}>
          <Text
            style={{
              width: 65,
              height: 15,
              fontFamily: 'Montserrat',
              fontSize: 12,
              fontStyle: 'normal',
              fontWeight: 'bold',
              lineHeight: 16,
              color: '#FFC107',
              top: -1,
              left: 9,
            }}>
            Reserved
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Transaksi;
