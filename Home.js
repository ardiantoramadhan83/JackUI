import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const Home = ({navigation, route}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#f6f8ff',
      }}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <View
            style={{
              width: Dimensions.get('screen').width,
              height: 275,
              backgroundColor: '#ffffff',
            }}>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 56,
                justifyContent: 'space-between',
                paddingLeft: 22,
                paddingRight: 34,
              }}>
              <TouchableOpacity>
                <Image
                  source={require('./assets/Profil.png')}
                  style={{
                    width: 45,
                    height: 45,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('./assets/Gembok.png')}
                  style={{
                    width: 24,
                    height: 24,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                left: 22,
                fontFamily: 'Montserrat',
                fontStyle: 'normal',
                fontWeight: '500',
                fontSize: 15,
                lineHeight: 36,
                color: '#034262',
              }}>
              Hello, Ardianto!
            </Text>
            <Text
              style={{
                left: 22,
                fontFamily: 'Montserrat',
                fontStyle: 'normal',
                fontWeight: '700',
                fontSize: 25,
                lineHeight: 33,
                color: '#0a0827',
              }}>
              Ingin merawat dan perbaiki sepatumu? Cari disini
            </Text>
            <View
              style={{
                paddingLeft: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingRight: 41,
                width: '100%',
              }}>
              <View
                style={{
                  width: 255,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 20,
                  justifyContent: 'space-between',
                  paddingLeft: 12,
                  paddingRight: 15,
                  borderRadius: 14,
                  backgroundColor: '#f6f8ff',
                  borderWidth: 0,
                  height: 45,
                }}>
                <Image
                  source={require('./assets/Search.png')}
                  style={{
                    width: 17,
                    height: 17,
                  }}
                />
                <TextInput
                  style={{
                    marginTop: 1,
                    width: '100%',
                    borderRadius: 8,
                    backgroundColor: '#F6F8FF',
                    paddingHorizontal: 10,
                  }}
                />
              </View>
              <View
                style={{
                  width: 45,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 20,
                  paddingLeft: 13,
                  marginLeft: 15,
                  borderRadius: 14,
                  backgroundColor: '#f6f8ff',
                  borderWidth: 0,
                  height: 45,
                }}>
                <TouchableOpacity>
                  <Image source={require('./assets/Filter.png')} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 15,
              marginTop: 42,
            }}>
            <ScrollView
              style={{
                flex: 1,
              }}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity>
                <View
                  style={{
                    width: 95,
                    height: 95,
                    borderRadius: 16,
                    backgroundColor: '#ffffff',
                    flexDirection: 'column',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={require('./assets/Sepatu.png')}
                    style={{
                      width: 45,
                      height: 45,
                      marginHorizontal: 25,
                    }}
                  />
                  <Text
                    style={{
                      fontFamily: 'Montserrat',
                      fontSize: 11,
                      fontStyle: 'normal',
                      fontWeight: '600',
                      lineHeight: 11,
                      display: 'flex',
                      textAlign: 'center',
                      color: '#BB2427',
                    }}>
                    Sepatu
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View
                  style={{
                    width: 95,
                    height: 95,
                    borderRadius: 16,
                    backgroundColor: '#ffffff',
                    flexDirection: 'column',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    marginHorizontal: 25,
                  }}>
                  <Image
                    source={require('./assets/Jaket.png')}
                    style={{
                      width: 45,
                      height: 45,
                      marginHorizontal: 25,
                    }}
                  />
                  <Text
                    style={{
                      fontFamily: 'Montserrat',
                      fontSize: 11,
                      fontStyle: 'normal',
                      fontWeight: '600',
                      lineHeight: 11,
                      display: 'flex',
                      textAlign: 'center',
                      color: '#BB2427',
                    }}>
                    Jaket
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View
                  style={{
                    width: 95,
                    height: 95,
                    borderRadius: 16,
                    backgroundColor: '#ffffff',
                    flexDirection: 'column',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={require('./assets/Tas.png')}
                    style={{
                      width: 45,
                      height: 45,
                      marginHorizontal: 25,
                    }}
                  />
                  <Text
                    style={{
                      fontFamily: 'Montserrat',
                      fontSize: 11,
                      fontStyle: 'normal',
                      fontWeight: '600',
                      lineHeight: 11,
                      display: 'flex',
                      textAlign: 'center',
                      color: '#BB2427',
                    }}>
                    Tas
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View
                  style={{
                    width: 95,
                    height: 95,
                    borderRadius: 16,
                    backgroundColor: '#ffffff',
                    flexDirection: 'column',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={require('./assets/FotoAI.png')}
                    style={{
                      width: 45,
                      height: 45,
                      marginHorizontal: 25,
                    }}
                  />
                  <Text
                    style={{
                      fontFamily: 'Montserrat',
                      fontSize: 11,
                      fontStyle: 'normal',
                      fontWeight: '600',
                      lineHeight: 11,
                      display: 'flex',
                      textAlign: 'center',
                      color: '#BB2427',
                    }}>
                    Vtuber
                  </Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 27,
              justifyContent: 'space-between',
              paddingHorizontal: 20,
            }}>
            <Text
              style={{
                fontFamily: 'Montserrat',
                fontSize: 15,
                fontStyle: 'normal',
                fontWeight: '600',
                lineHeight: 15,
                textAlign: 'center',
                color: '#0A0827',
              }}>
              Rekomendasi Terdekat
            </Text>
            <TouchableOpacity>
              <Text
                style={{
                  fontFamily: 'Montserrat',
                  fontSize: 15,
                  fontStyle: 'normal',
                  fontWeight: '600',
                  lineHeight: 15,
                  textAlign: 'center',
                  color: '#E64C3C',
                }}>
                View All
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <View
              style={{
                width: 335,
                height: 133,
                marginHorizontal: 13,
                marginTop: 25,
                backgroundColor: '#ffffff',
                borderRadius: 9,
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Image
                source={require('./assets/Business.png')}
                style={{
                  height: 121,
                  width: 80,
                  marginLeft: 6,
                  borderRadius: 5,
                }}
              />
              <View
                style={{
                  width: 234,
                  height: 133,
                  marginHorizontal: 15,
                  backgroundColor: '#ffffff',
                  borderRadius: 9,
                  justifyContent: 'flex-start',
                  flexDirection: 'column',
                }}>
                <View
                  style={{
                    width: 234,
                    height: 12,
                    marginTop: 14,
                    backgroundColor: '#ffffff',
                    borderRadius: 9,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Image
                    source={require('./assets/Rating.png')}
                    style={{
                      width: 50,
                      height: 7,
                    }}
                  />
                  <Image
                    source={require('./assets/Love.png')}
                    style={{
                      width: 13,
                      height: 12,
                      right: 9,
                    }}
                  />
                </View>
                <Text
                  style={{
                    height: 20,
                    width: 55,
                    fontFamily: 'Montserrat',
                    fontSize: 10,
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 20,
                    color: '#d8d8d8',
                  }}>
                  4.8 Ratings
                </Text>
                <Text
                  style={{
                    width: 255,
                    height: 34,
                    fontFamily: 'Montserrat',
                    fontSize: 12,
                    fontStyle: 'normal',
                    fontWeight: '600',
                    lineHeight: 17,
                    letterSpacing: 1,
                    color: '#201f26',
                  }}>
                  Jack Repair Gejayan
                </Text>
                <Text
                  style={{
                    width: 226,
                    height: 20,
                    fontFamily: 'Montserrat',
                    fontSize: 12,
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 20,
                    color: '#d8d8d8',
                    top: -20,
                  }}>
                  Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .
                </Text>
                <View
                  style={{
                    width: 58,
                    height: 21,
                    marginRight: 13,
                    borderRadius: 10,
                    backgroundColor: '#e64c3c',
                    opacity: 0.2,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      width: 47,
                      height: 15,
                      fontFamily: 'Montserrat',
                      fontSize: 12,
                      fontStyle: 'normal',
                      fontWeight: '700',
                      lineHeight: 17,
                      letterSpacing: 1,
                      color: '#ea3d3d',
                      left: 5,
                    }}>
                    TUTUP
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Detail')}>
            <View
              style={{
                width: 335,
                height: 133,
                marginHorizontal: 13,
                marginTop: 25,
                backgroundColor: '#ffffff',
                borderRadius: 9,
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Image
                source={require('./assets/Tempat2.png')}
                style={{
                  height: 121,
                  width: 80,
                  marginLeft: 6,
                  borderRadius: 5,
                }}
              />
              <View
                style={{
                  width: 234,
                  height: 133,
                  marginHorizontal: 15,
                  backgroundColor: '#ffffff',
                  borderRadius: 9,
                  justifyContent: 'flex-start',
                  flexDirection: 'column',
                }}>
                <View
                  style={{
                    width: 234,
                    height: 12,
                    marginTop: 14,
                    backgroundColor: '#ffffff',
                    borderRadius: 9,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Image
                    source={require('./assets/Rating.png')}
                    style={{
                      width: 50,
                      height: 7,
                    }}
                  />
                  <Image
                    source={require('./assets/LovePutih.png')}
                    style={{
                      width: 13,
                      height: 12,
                      right: 9,
                    }}
                  />
                </View>
                <Text
                  style={{
                    height: 20,
                    width: 55,
                    fontFamily: 'Montserrat',
                    fontSize: 10,
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 20,
                    color: '#d8d8d8',
                  }}>
                  4.7 Ratings
                </Text>
                <Text
                  style={{
                    width: 255,
                    height: 34,
                    fontFamily: 'Montserrat',
                    fontSize: 12,
                    fontStyle: 'normal',
                    fontWeight: '600',
                    lineHeight: 17,
                    letterSpacing: 1,
                    color: '#201f26',
                  }}>
                  Jack Repair Gejayan
                </Text>
                <Text
                  style={{
                    width: 226,
                    height: 20,
                    fontFamily: 'Montserrat',
                    fontSize: 12,
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 20,
                    color: '#d8d8d8',
                    top: -20,
                  }}>
                  Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .
                </Text>
                <View
                  style={{
                    width: 58,
                    height: 21,
                    marginRight: 13,
                    borderRadius: 10,
                    backgroundColor: '#11a84e',
                    opacity: 0.12,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      width: 47,
                      height: 15,
                      fontFamily: 'Montserrat',
                      fontSize: 12,
                      fontStyle: 'normal',
                      fontWeight: '700',
                      lineHeight: 17,
                      letterSpacing: 1,
                      color: '#11a84e',
                      left: 7,
                    }}>
                    BUKA
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default Home;
