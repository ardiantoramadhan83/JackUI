import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const FAQ = ({navigation}) => {
  const [show, setShow] = useState(null);
  return (
    <View style={{flex: 1, backgroundColor: '#f6f8ff'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 3,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={20} />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 17,
            color: '#034262',
            fontWeight: 'bold',
          }}>
          FAQ
        </Text>
      </View>
      <View style={{marginTop: 20}}>
        <TouchableOpacity
          onPress={() => setShow(show === 2 ? null : 2)}
          style={{
            paddingRight: 14,
            flexDirection: 'row',
            backgroundColor: 'white',
            margin: 7,
            padding: 14,
            justifyContent: 'space-between',
          }}>
          <View style={{}}>
            <Text
              style={{
                fontFamily: 'Montserrat',
                fontSize: 17,
                color: '#034262',
                fontWeight: 'bold',
              }}>
              World
            </Text>
            {show === 2 ? (
              <Text
                style={{
                  fontFamily: 'Montserrat',
                  fontSize: 17,
                  color: '#034262',
                }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit?
              </Text>
            ) : null}
          </View>
          <View>
            <Icon name={show === 2 ? 'up' : 'down'} size={20} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setShow(show === 3 ? null : 3)}
          style={{
            paddingRight: 14,
            flexDirection: 'row',
            backgroundColor: 'white',
            margin: 7,
            padding: 14,
            justifyContent: 'space-between',
          }}>
          <View style={{}}>
            <Text
              style={{
                fontFamily: 'Montserrat',
                fontSize: 17,
                color: '#034262',
                fontWeight: 'bold',
              }}>
              World
            </Text>
            {show === 3 ? (
              <Text
                style={{
                  fontFamily: 'Montserrat',
                  fontSize: 17,
                  color: '#034262',
                }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit?
              </Text>
            ) : null}
          </View>
          <View>
            <Icon name={show === 3 ? 'up' : 'down'} size={20} />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FAQ;
