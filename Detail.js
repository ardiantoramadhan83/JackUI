import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const Detail = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <Image
          source={require('./assets/Pic2.png')}
          style={{
            width: Dimensions.get('window').width,
            height: 300,
          }}
        />
        <View
          style={{
            width: '100%',
            backgroundColor: '#fff',
            borderTopLeftRadius: 19,
            borderTopRightRadius: 19,
            paddingHorizontal: 20,
            paddingTop: 38,
            marginTop: -20,
          }}></View>
        <Text
          style={{
            left: 22,
            fontFamily: 'Montserrat',
            fontStyle: 'normal',
            fontWeight: '700',
            fontSize: 25,
            lineHeight: 33,
            color: '#0a0827',
          }}>
          Jack Repair Seturan
        </Text>
        <Image
          source={require('./assets/Rating.png')}
          style={{
            width: 70,
            height: 13.5,
            left: 25,
            marginTop: 10,
          }}
        />
        <Text
          style={{
            fontSize: 13,
            color: '#333',
            paddingLeft: 100,
            paddingRight: 10,
            textAlign: 'center',
            right: 70,
            margin: 10,
          }}>
          Jalan Affandi (Gejayan), No 15, Sleman, Yogyakarta, 55384
        </Text>
        <Image
          source={require('./assets/Lokasi.png')}
          style={{
            width: 16,
            height: 20,
            left: 25,
            marginTop: -40,
          }}
        />
        <TouchableOpacity>
          <Text
            style={{
              fontSize: 13,
              color: 'blue',
              paddingRight: 20,
              textAlign: 'right',
              top: -20,
              fontFamily: 'Montserrat',
            }}>
            Lihat map
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 16,
            color: '#333',
            paddingLeft: 100,
            paddingRight: 10,
            textAlign: 'center',
            right: 100,
            margin: 10,
            top: -7,
          }}>
          09:00 - 21:00
        </Text>
        <View
          style={{
            width: 58,
            height: 21,
            left: 20,
            borderRadius: 10,
            backgroundColor: '#11a84e',
            opacity: 0.4,
            justifyContent: 'center',
            alignItems: 'center',
            top: -39,
          }}>
          <Text
            style={{
              width: 47,
              height: 15,
              fontFamily: 'Montserrat',
              fontSize: 12,
              fontStyle: 'normal',
              fontWeight: '700',
              lineHeight: 16,
              letterSpacing: 1,
              color: 'green',
              left: 7,
            }}>
            BUKA
          </Text>
        </View>
        <View>
          <Text
            style={{
              left: 22,
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: '500',
              fontWeight: 'bold',
              fontSize: 16,
              lineHeight: 17.21,
              color: '#0a0827',
            }}>
            Deskripsi
          </Text>
          <Text
            style={{
              fontSize: 14,
              color: '#333',
              paddingLeft: 100,
              paddingRight: 10,
              textAlign: 'left',
              right: 87,
              margin: 10,
            }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
            gravida mattis arcu interdum lectus egestas scelerisque. Blandit
            porttitor diam viverra amet nulla sodales aliquet est. Donec enim
            turpis rhoncus quis integer. Ullamcorper morbi donec tristique
            condimentum ornare imperdiet facilisi pretium molestie.
          </Text>
          <Text
            style={{
              left: 22,
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: '500',
              fontWeight: 'bold',
              fontSize: 16,
              lineHeight: 17.21,
              color: '#0a0827',
              marginTop: 10,
            }}>
            Range Biaya
          </Text>
          <Text
            style={{
              fontSize: 16,
              color: '#333',
              paddingLeft: 100,
              paddingRight: 10,
              textAlign: 'left',
              right: 87,
              margin: 10,
            }}>
            Rp 20.000 - Rp 100.000
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('FormulirPemesanan')}
            style={{
              width: '80%',
              marginTop: 50,
              backgroundColor: '#BB2427',
              borderRadius: 10,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
              left: 38,
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 20,
                fontWeight: 'bold',
              }}>
              Repair Disini
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Detail;
