import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import Iconaize from 'react-native-vector-icons/Ionicons';

const Transaksi2 = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            padding: 19,
            elevation: 3,
            backgroundColor: 'white',
          }}>
          <TouchableOpacity
            style={{paddingRight: 14}}
            onPress={() => navigation.goBack()}>
            <Icon name="arrowleft" size={20} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: 400,
            height: 250,
            marginTop: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            padding: 10,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 16,
              fontStyle: 'normal',
              textAlign: 'center',
              top: -85,
              left: -16,
              color: 'gray',
            }}>
            20 Desember 2020
          </Text>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 16,
              fontStyle: 'normal',
              textAlign: 'left',
              top: -85,
              left: -90,
              color: 'gray',
            }}>
            09:00
          </Text>
        </View>
        <View>
          <Text
            style={{
              width: 340,
              height: 100,
              fontSize: 45,
              fontStyle: 'normal',
              textAlign: 'center',
              top: -170,
              color: '#201F26',
              left: 30,
              fontWeight: 'bold',
            }}>
            CS122001
          </Text>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 16,
              fontStyle: 'normal',
              textAlign: 'left',
              top: -213,
              left: 150,
              color: 'black',
            }}>
            Kode Reservasi
          </Text>
        </View>
        <View>
          <Text
            style={{
              width: 200,
              height: 34,
              fontFamily: 'Montserrat',
              fontSize: 15,
              color: '#034262',
              textAlign: 'center',
              top: -180,
              left: 105,
            }}>
            Sebutkan Kode Reservasi saat tiba di outlet
          </Text>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Checkout')}>
          <View
            style={{
              width: 350,
              height: 150,
              marginHorizontal: 5,
              marginTop: -90,
              borderRadius: 9,
              justifyContent: 'space-between',
              alignItems: 'center',
              left: 15,
              flexDirection: 'row',
              padding: 19,
              elevation: 3,
              backgroundColor: 'white',
            }}>
            <Image
              source={require('./assets/SepatuPink.png')}
              style={{
                height: 100,
                width: 100,
                marginLeft: -4,
              }}
            />
            <Text
              style={{
                width: 230,
                height: 34,
                fontFamily: 'Lucida Console',
                fontSize: 13,
                fontStyle: 'normal',
                letterSpacing: 1,
                color: '#201f26',
                top: -23,
                marginHorizontal: 1,
                textAlign: 'center',
                fontWeight: 'bold',
              }}>
              New Balance - Pink Abu - 40
            </Text>
            <Text
              style={{
                width: 230,
                height: 34,
                fontStyle: 'normal',
                textAlign: 'left',
                right: 214,
                top: 10,
              }}>
              Cuci Sepatu
            </Text>
            <Text
              style={{
                width: 230,
                height: 34,
                fontStyle: 'normal',
                textAlign: 'left',
                right: 445,
                top: 45,
              }}>
              Note : -
            </Text>
          </View>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'center',
              fontSize: 18,
              top: -200,
              right: 70,
            }}>
            Barang
          </Text>
        </TouchableOpacity>
        <View>
          <Text
            style={{
              width: 230,
              height: 34,
              textAlign: 'center',
              right: 28,
              fontSize: 16,
              top: -5,
            }}>
            Status Pemesanan
          </Text>
          <View
            style={{
              width: 350,
              height: 90,
              marginHorizontal: 5,
              marginTop: 1,
              borderRadius: 9,
              justifyContent: 'space-between',
              alignItems: 'center',
              left: 15,
              flexDirection: 'row',
              padding: 19,
              elevation: 3,
              backgroundColor: 'white',
            }}>
            <Iconaize name="ellipse" size={15} color={'red'} />
            <Text
              style={{
                width: 230,
                height: 34,
                textAlign: 'center',
                right: 40,
                fontSize: 16,
                top: -10,
                fontWeight: 'bold',
              }}>
              Telah Reservasi
            </Text>
            <Text
              style={{
                width: 230,
                height: 34,
                textAlign: 'center',
                right: 257,
                fontSize: 16,
                top: 15,
                fontWeight: 'bold',
              }}>
              20 Desember 2020
            </Text>
            <Text
              style={{
                width: 230,
                height: 34,
                textAlign: 'center',
                right: 300,
                fontSize: 16,
                top: 5,
              }}>
              09:00
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Transaksi2;
