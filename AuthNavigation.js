import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Login from './Login';
import Register from './Register';
import ForgotPass from './ForgotPass';
import Home from './Home';

const Stack = createStackNavigator();

function AuthNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="ForgotPass" component={ForgotPass} />
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  );
}

export default AuthNavigation;
