import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Iconizaz from 'react-native-vector-icons/MaterialCommunityIcons';

const Checkout = ({navigation, route}) => {
  const [isChecked, setIschecked] = useState(false);
  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 3,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={20} />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 17,
            color: '#034262',
            fontWeight: 'bold',
          }}>
          Checkout
        </Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: 400,
            height: 170,
            marginTop: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            padding: 16,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 16,
              fontStyle: 'normal',
              textAlign: 'left',
              top: -45,
              left: 20,
              color: 'gray',
            }}>
            Data Costumer :
          </Text>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 16,
              fontStyle: 'normal',
              right: 320,
              fontWeight: 'bold',
              fontFamily: 'Montserrat',
              top: -10,
            }}>
            Adalah Saya (08999999999)
          </Text>
          <Text
            style={{
              width: 340,
              height: 50,
              fontSize: 16,
              fontStyle: 'normal',
              right: 660,
              fontWeight: 'bold',
              fontFamily: 'Montserrat',
              top: 20,
            }}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text
            style={{
              width: 340,
              height: 50,
              fontSize: 16,
              fontStyle: 'normal',
              right: 1000,
              fontWeight: 'bold',
              fontFamily: 'Montserrat',
              top: 72,
            }}>
            gantengdoang@dipanggang.com
          </Text>
        </View>
        <View
          style={{
            width: 400,
            height: 110,
            marginTop: 10,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            padding: 10,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'gray',
              top: -30,
              left: 20,
            }}>
            Alamat Outlet Tujuan :
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              top: 1,
              right: 150,
            }}>
            Jack Repair - Seturan (027-343457)
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              top: 25,
              right: 418,
            }}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View
          style={{
            width: 400,
            height: 250,
            marginTop: 10,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            padding: 10,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'gray',
              top: -100,
              left: 20,
            }}>
            Barang
          </Text>
          <Image
            source={require('./assets/SepatuPink.png')}
            style={{
              height: 100,
              width: 100,
              right: 35,
              top: -15,
            }}
          />
          <Text
            style={{
              width: 230,
              height: 34,
              fontFamily: 'Lucida Console',
              fontSize: 13,
              fontStyle: 'normal',
              letterSpacing: 1,
              color: '#201f26',
              top: -40,
              left: -30,
              textAlign: 'center',
              fontWeight: 'bold',
            }}>
            New Balance - Pink Abu - 40
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              right: 243,
              top: -10,
            }}>
            Cuci Sepatu
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              right: 472,
              top: 20,
            }}>
            Note : -
          </Text>
          <Text
            style={{
              fontSize: 20,
              fontStyle: 'normal',
              textAlign: 'left',
              right: 810,
              top: 85,
            }}>
            1 Pasang
          </Text>
          <Text
            style={{
              fontSize: 20,
              fontStyle: 'normal',
              textAlign: 'left',
              fontWeight: 'bold',
              color: 'black',
              right: 680,
              top: 85,
            }}>
            @Rp.50.000
          </Text>
        </View>
        <View
          style={{
            width: 400,
            height: 170,
            backgroundColor: '#fff',
            borderTopLeftRadius: 1,
            borderTopRightRadius: 1,
            paddingHorizontal: 1,
            top: 10,
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'gray',
              top: 10,
              left: 35,
            }}>
            Rincian Pembayaran
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'black',
              fontWeight: 'bold',
              top: 35,
              left: 35,
            }}>
            Cuci Sepatu
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#FFC107',
              textAlign: 'center',
              top: 12,
              left: 10,
            }}>
            X1 Pasang
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'black',
              textAlign: 'right',
              fontWeight: 'bold',
              top: -10,
              right: 20,
            }}>
            Rp. 30.000
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'black',
              fontWeight: 'bold',
              top: 1,
              left: 36,
            }}>
            Biaya Antar
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'black',
              textAlign: 'right',
              fontWeight: 'bold',
              top: -20,
              right: 20,
            }}>
            Rp. 3000
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'black',
              fontWeight: 'bold',
              top: 1,
              left: 36,
            }}>
            Total
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              textAlign: 'right',
              fontWeight: 'bold',
              top: -20,
              right: 20,
            }}>
            Rp. 33.000
          </Text>
        </View>
        <View
          style={{
            width: 390,
            height: 200,
            borderColor: 'white',
            borderTopLeftRadius: 1,
            borderTopRightRadius: 1,
            paddingHorizontal: 1,
            top: 20,
            borderWidth: 1,
          }}>
          <ScrollView
            style={{
              flex: 1,
              backgroundColor: 'white',
              height: 100,
            }}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <TouchableOpacity>
              <View
                style={{
                  width: 130,
                  height: 95,
                  color: 'black',
                  borderWidth: 1,
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  marginTop: 60,
                  marginLeft: 40,
                  paddingLeft: 1,
                }}>
                <Icon
                  style={{
                    left: 40,
                  }}
                  name="checkcircle"
                  size={20}
                  color={'blue'}
                />
                <Iconizaz
                  style={{
                    left: 5,
                  }}
                  name="bank-transfer"
                  size={50}
                  color={'black'}
                />
                <Text
                  style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    fontStyle: 'normal',
                    display: 'flex',
                    textAlign: 'center',
                    color: 'black',
                    left: 1,
                    top: 2,
                  }}>
                  Back Transfer
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View
                style={{
                  width: 130,
                  height: 95,
                  color: 'black',
                  borderWidth: 1,
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  marginTop: 60,
                  marginLeft: 40,
                }}>
                <Image
                  source={require('./assets/OVO.png')}
                  style={{
                    width: 120,
                    height: 37,
                    marginHorizontal: 25,
                    marginTop: 20,
                  }}
                />
                <Text
                  style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    height: 30,
                    width: 40,
                    fontStyle: 'normal',
                    color: 'black',
                    left: 1,
                    top: 9,
                  }}>
                  OVO
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View
                style={{
                  width: 130,
                  height: 95,
                  color: 'black',
                  borderWidth: 1,
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  marginTop: 60,
                  marginLeft: 40,
                }}>
                <Image
                  source={require('./assets/Dana.png')}
                  style={{
                    width: 120,
                    height: 37,
                    marginHorizontal: 25,
                    marginTop: 20,
                  }}
                />
                <Text
                  style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    height: 30,
                    width: 40,
                    fontStyle: 'normal',
                    color: 'black',
                    left: 1,
                    top: 9,
                  }}>
                  Dana
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View
                style={{
                  width: 130,
                  height: 95,
                  color: 'black',
                  borderWidth: 1,
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  marginTop: 60,
                  marginLeft: 40,
                }}>
                <Icon
                  style={{
                    left: 5,
                  }}
                  name="creditcard"
                  size={50}
                  color={'black'}
                />
                <Text
                  style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    fontStyle: 'normal',
                    display: 'flex',
                    textAlign: 'center',
                    color: 'black',
                    left: 1,
                    top: 2,
                  }}>
                  Kartu Kredit
                </Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'gray',
              top: -160,
              left: 35,
            }}>
            Pilih Mode Pembayaran
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('')}
          style={{
            width: '100%',
            marginTop: 30,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Pesan Sekarang
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Checkout;
