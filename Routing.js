import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import SpaceScreen from './SpaceScreen';
import AuthNavigation from './AuthNavigation';
import MyTabBar from './BotNav';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="SpaceScreen" component={SpaceScreen} />
        <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
        <Stack.Screen name="BotNav" component={MyTabBar} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
