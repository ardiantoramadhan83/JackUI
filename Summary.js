import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
const Summary = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            padding: 19,
            elevation: 3,
            backgroundColor: 'white',
          }}>
          <TouchableOpacity
            style={{paddingRight: 14}}
            onPress={() => navigation.goBack()}>
            <Icon name="arrowleft" size={20} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              fontWeight: 'bold',
            }}>
            Summary
          </Text>
        </View>
        <View
          style={{
            width: 400,
            height: 170,
            marginTop: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            padding: 10,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 16,
              fontStyle: 'normal',
              textAlign: 'left',
              top: -45,
              left: 20,
              color: 'gray',
            }}>
            Data Costumer :
          </Text>
          <Text
            style={{
              width: 340,
              height: 34,
              fontSize: 16,
              fontStyle: 'normal',
              right: 320,
              fontWeight: 'bold',
              fontFamily: 'Montserrat',
              top: -10,
            }}>
            Adalah Saya (08999999999)
          </Text>
          <Text
            style={{
              width: 340,
              height: 50,
              fontSize: 16,
              fontStyle: 'normal',
              right: 660,
              fontWeight: 'bold',
              fontFamily: 'Montserrat',
              top: 20,
            }}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text
            style={{
              width: 340,
              height: 50,
              fontSize: 16,
              fontStyle: 'normal',
              right: 1000,
              fontWeight: 'bold',
              fontFamily: 'Montserrat',
              top: 72,
            }}>
            gantengdoang@dipanggang.com
          </Text>
        </View>
        <View
          style={{
            width: 400,
            height: 110,
            marginTop: 10,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            padding: 10,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'gray',
              top: -30,
              left: 20,
            }}>
            Alamat Outlet Tujuan :
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              top: 1,
              right: 150,
            }}>
            Jack Repair - Seturan (027-343457)
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              top: 25,
              right: 418,
            }}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View
          style={{
            width: 400,
            height: 170,
            marginTop: 10,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            padding: 10,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: 'gray',
              top: -60,
              left: 20,
            }}>
            Barang
          </Text>
          <Image
            source={require('./assets/SepatuPink.png')}
            style={{
              height: 100,
              width: 100,
              right: 35,
              top: 20,
            }}
          />
          <Text
            style={{
              width: 230,
              height: 34,
              fontFamily: 'Lucida Console',
              fontSize: 13,
              fontStyle: 'normal',
              letterSpacing: 1,
              color: '#201f26',
              top: -10,
              left: -30,
              textAlign: 'center',
              fontWeight: 'bold',
            }}>
            New Balance - Pink Abu - 40
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              right: 243,
              top: 16,
            }}>
            Cuci Sepatu
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              right: 472,
              top: 45,
            }}>
            Note : -
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('Berhasil')}
          style={{
            width: '80%',
            marginTop: 100,
            backgroundColor: '#BB2427',
            borderRadius: 10,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
            left: 38,
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 20,
              fontWeight: 'bold',
            }}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Summary;
