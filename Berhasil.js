import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

const Berhasil = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="x" size={20} />
        </TouchableOpacity>
      </View>
      <Text
        style={{
          fontFamily: 'Montserrat',
          fontSize: 25,
          color: '#034262',
          fontWeight: 'bold',
          textAlign: 'center',
          top: 60,
          left: 3,
        }}>
        Reservasi Berhasil
      </Text>
      <View
        style={{
          width: 200,
          height: 200,
          borderRadius: 60,
          backgroundColor: '#00800040',
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
          marginHorizontal: 100,
          top: 100,
        }}>
        <Image
          source={require('./assets/CeklisHijau.png')}
          style={{
            width: 115,
            height: 85,
            marginHorizontal: 25,
          }}
        />
      </View>
      <Text
        style={{
          fontFamily: 'Montserrat',
          fontSize: 23,
          color: '#034262',
          textAlign: 'center',
          top: 140,
          left: 3,
        }}>
        Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
      </Text>
      <TouchableOpacity
        onPress={() => navigation.navigate('BotNav')}
        style={{
          width: '80%',
          marginTop: 260,
          backgroundColor: '#BB2427',
          borderRadius: 10,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          left: 41,
        }}>
        <Text
          style={{
            color: '#fff',
            fontSize: 20,
            fontWeight: 'bold',
          }}>
          Lihat Kode Reservasi
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Berhasil;
