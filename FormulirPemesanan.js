import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Iconz from 'react-native-vector-icons/Fontisto';
import Iconiz from 'react-native-vector-icons/MaterialIcons';
import Iconiza from 'react-native-vector-icons/Feather';

const FormulirPemesanan = ({navigation, route}) => {
  const [isChecked, setIschecked] = useState(false);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 3,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={20} />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 17,
            color: '#034262',
            fontWeight: 'bold',
          }}>
          Formulir Pemesanan
        </Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              paddingHorizontal: 20,
              paddingTop: 38,
              top: 2,
            }}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Merek</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Masukan Merek Barang" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <Text style={{color: 'red', fontWeight: 'bold', top: 20}}>
              Warna
            </Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Warna Barang , cth: Merah-Putih" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 30,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <Text style={{color: 'red', fontWeight: 'bold', top: 20}}>
              Ukuran
            </Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Cth: S, M, L/39, 40" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 30,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <Text style={{color: 'red', fontWeight: 'bold', top: 20}}>
              Photo
            </Text>
            <TouchableOpacity>
              <Image
                source={require('./assets/KotakMerah.png')}
                style={{
                  width: 100,
                  height: 100,
                  marginTop: 50,
                }}
              />
              <Image
                source={require('./assets/Camera.png')}
                style={{
                  left: 35,
                  alignItems: 'center',
                  top: -70,
                }}
              />
              <Text
                style={{
                  color: 'red',
                  textAlign: 'left',
                  marginTop: -40,
                  left: 15,
                }}>
                Add Photo
              </Text>
            </TouchableOpacity>
            <View>
              <TouchableOpacity
                onPress={() =>
                  setIschecked(
                    isChecked == 'ganti sol spatu' ? false : 'ganti sol spatu',
                  )
                }
                style={{marginTop: 30, left: 10}}>
                <Iconz
                  name={
                    isChecked == 'ganti sol spatu'
                      ? 'check'
                      : 'checkbox-passive'
                  }
                  size={20}
                />
                <Text
                  style={{
                    color: 'black',
                    paddingLeft: 40,
                    top: -20,
                    fontSize: 16,
                  }}>
                  Ganti Sol Sepatu
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  setIschecked(
                    isChecked == 'jahit jaket' ? false : 'jahit jaket',
                  )
                }
                style={{marginTop: 10, left: 10}}>
                <Iconz
                  name={
                    isChecked == 'jahit jaket' ? 'check' : 'checkbox-passive'
                  }
                  size={20}
                />
                <Text
                  style={{
                    color: 'black',
                    paddingLeft: 40,
                    top: -20,
                    fontSize: 16,
                  }}>
                  Jahit Sepatu
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  setIschecked(
                    isChecked == 'repaint sepatu' ? false : 'repaint sepatu',
                  )
                }
                style={{marginTop: 10, left: 10}}>
                <Iconz
                  name={
                    isChecked == 'repaint sepatu' ? 'check' : 'checkbox-passive'
                  }
                  size={20}
                />
                <Text
                  style={{
                    color: 'black',
                    paddingLeft: 40,
                    top: -20,
                    fontSize: 16,
                  }}>
                  Repaint Sepatu
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  setIschecked(
                    isChecked == 'cuci sepatu' ? false : 'cuci sepatu',
                  )
                }
                style={{marginTop: 10, left: 10}}>
                <Iconz
                  name={
                    isChecked == 'cuci sepatu' ? 'check' : 'checkbox-passive'
                  }
                  size={20}
                />
                <Text
                  style={{
                    color: 'black',
                    paddingLeft: 40,
                    top: -20,
                    fontSize: 16,
                  }}>
                  Cuci Sepatu
                </Text>
              </TouchableOpacity>
            </View>
            <Text style={{color: 'red', fontWeight: 'bold', top: 20}}>
              Catatan
            </Text>
            <TextInput
              placeholder="Cth: S, M, L/39, 40"
              style={{
                marginTop: 30,
                width: '100%',
                height: 93,
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 10}}>
              Kupon Promo
            </Text>
            {route?.params ? (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderWidth: 1,
                  borderColor: 'red',
                  borderRadius: 8,
                  paddingLeft: 10,
                  paddingRight: 16,
                  paddingVertical: 17,
                  alignContent: 'center',
                  marginTop: 10,
                }}
                onPress={() => navigation.navigate('KodePromo')}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat',
                        fontWeight: 'bold',
                        marginRight: 5,
                      }}>
                      {route.params}
                    </Text>
                    <Iconiz name="verified" size={20} color="green" />
                  </View>
                </View>
                <Iconiza name="x" size={20} color="red" />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderWidth: 1,
                  borderColor: 'red',
                  borderRadius: 8,
                  paddingLeft: 15,
                  paddingRight: 15,
                  paddingVertical: 17,
                  alignContent: 'center',
                  marginTop: 40,
                }}
                onPress={() => navigation.navigate('KodePromo')}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat',
                        fontWeight: 'bold',
                        marginRight: 5,
                      }}>
                      Pilih Kupon Promo
                    </Text>
                  </View>
                </View>
                <Iconiz name="keyboard-arrow-right" size={25} color="red" />
              </TouchableOpacity>
            )}
            <TouchableOpacity
              onPress={() => navigation.navigate('Keranjang')}
              style={{
                width: '100%',
                marginTop: 30,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Masukan Keranjang
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default FormulirPemesanan;
