import React from 'react';
import { 
   View, 
   Text,
   ScrollView,
   KeyboardAvoidingView,
   Image,
   TextInput,
   TouchableOpacity,
   StyleSheet,
   Dimensions
} from 'react-native';
const Login = ({
   navigation,
   route
}) => {

   const ForgotPass = () => {
      navigation.navigate('ForgotPass');
    };

   return(
      <View style={{flex: 1, backgroundColor: '#fff'}}>
         <ScrollView //component yang digunakan agar tampilan kita bisa discroll
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: 10}}
         >
            
            <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
               behavior='padding' //tampilan form atau text input
               enabled
               keyboardVerticalOffset={-500}
            >
               <Image
                  source={require('./assets/Smile.png')} //load atau panggil asset image dari local
                  style={{
                     width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
                     height: 200
                  }}
               />
               <View style={{                  
                  width: '100%',
                  backgroundColor: '#fff',
                  borderTopLeftRadius: 19,
                  borderTopRightRadius: 19,
                  paddingHorizontal: 20,
                  paddingTop: 38,
                  marginTop: -20
               }}>
                  <Text style={{color: 'red', fontWeight: 'bold'}}>
                     Email
                  </Text> 
                  <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                     placeholder='Masukkan Email' //pada tampilan ini, kita ingin user memasukkan email
                     style={{
                        marginTop: 15,
                        width: '100%',
                        borderRadius: 8,
                        backgroundColor: '#F6F8FF',
                        paddingHorizontal: 10
                     }}
                     keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
                  />
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}
                     style={{
                        width: '100%',
                        marginTop: 30,
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center'
                     }}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Submit
                     </Text>
                  </TouchableOpacity>
            </KeyboardAvoidingView>
            </ScrollView>
            </View>
   )
}
export default Login;
