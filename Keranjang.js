import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import Iconiza from 'react-native-vector-icons/Feather';

const Keranjang = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 3,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={20} />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 17,
            color: '#034262',
            fontWeight: 'bold',
          }}>
          Keranjang
        </Text>
      </View>
      <TouchableOpacity>
        <View
          style={{
            width: 350,
            height: 150,
            marginHorizontal: 5,
            marginTop: 25,
            borderRadius: 9,
            justifyContent: 'space-between',
            alignItems: 'center',
            left: 15,
            flexDirection: 'row',
            padding: 19,
            elevation: 3,
            backgroundColor: 'white',
          }}>
          <Image
            source={require('./assets/SepatuPink.png')}
            style={{
              height: 100,
              width: 100,
              marginLeft: -4,
            }}
          />
          <Text
            style={{
              width: 230,
              height: 34,
              fontFamily: 'Lucida Console',
              fontSize: 13,
              fontStyle: 'normal',
              letterSpacing: 1,
              color: '#201f26',
              top: -23,
              marginHorizontal: 1,
              textAlign: 'center',
              fontWeight: 'bold',
            }}>
            New Balance - Pink Abu - 40
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              right: 214,
              top: 10,
            }}>
            Cuci Sepatu
          </Text>
          <Text
            style={{
              width: 230,
              height: 34,
              fontStyle: 'normal',
              textAlign: 'left',
              right: 445,
              top: 45,
            }}>
            Note : -
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View
          style={{
            marginLeft: 130,
            top: 50,
          }}>
          <Iconiza name="plus-square" size={25} color="red" />
          <Text
            style={{
              color: 'red',
              fontSize: 16,
              fontWeight: 'bold',
              left: 35,
              top: -24,
            }}>
            Tambah Barang
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate('Summary')}
        style={{
          width: '80%',
          marginTop: 350,
          backgroundColor: '#BB2427',
          borderRadius: 10,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          left: 38,
        }}>
        <Text
          style={{
            color: '#fff',
            fontSize: 20,
            fontWeight: 'bold',
          }}>
          Selanjutnya
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Keranjang;
