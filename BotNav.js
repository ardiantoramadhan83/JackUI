import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import MaterialIcon from 'react-native-vector-icons/AntDesign';

import Home from './Home';
import Profil from './Profil';
import Transaksi from './Transaksi';

const Tab = createBottomTabNavigator();

export default function MyTabBar() {
  return (
    <Tab.Navigator
      tabBar={props => <MyTab {...props} />}
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Transaksi" component={Transaksi} />
      <Tab.Screen name="Profil" component={Profil} />
    </Tab.Navigator>
  );
}
function MyTab({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row', backgroundColor: '#fff'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const LabelIcon = {
          Home: 'home',
          Transaksi: 'profile',
          Profil: 'user',
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              margin: 5,
              backgroundColor: '#fff',
              alignItems: 'center',
            }}>
            <MaterialIcon name={LabelIcon[label]} size={20} color="red" />
            <Text style={{color: isFocused ? '#673ab7' : '#222'}}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
