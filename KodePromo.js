import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';

const KodePromo = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 8,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={20} />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 17,
            color: '#034262',
            fontWeight: 'bold',
          }}>
          Kode Promo
        </Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <SafeAreaView
          style={{
            flex: 1,
            paddingTop: StatusBar.currentHeight,
          }}>
          <View
            style={{
              width: 400,
              height: 100,
              marginTop: -20,
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
              padding: 10,
              backgroundColor: 'white',
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('Berhasil')}
              style={{
                width: '40%',
                marginTop: -2,
                backgroundColor: '#BB2427',
                borderRadius: 10,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
                left: 220,
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 17,
                  fontWeight: 'bold',
                }}>
                Gunakan
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('KodePromo2')}>
            <View
              style={{
                width: 350,
                height: 150,
                marginHorizontal: 5,
                marginTop: 40,
                borderRadius: 9,
                justifyContent: 'space-between',
                alignItems: 'center',
                left: 15,
                flexDirection: 'row',
                padding: 19,
                elevation: 3,
                backgroundColor: 'white',
              }}>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  fontWeight: 'bold',
                  textAlign: 'auto',
                  fontSize: 15,
                  left: 140,
                  color: 'black',
                  top: 30,
                  lineHeight: 17,
                }}>
                Promo Cashback Hingga 30rb
              </Text>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  textAlign: 'justify',
                  fontSize: 15,
                  left: 140,
                  color: 'gray',
                  top: 80,
                  lineHeight: 17,
                }}>
                09 s/d 15 Maret 2021
              </Text>
            </View>
            <View
              style={{
                width: 120,
                height: 120,
                borderRadius: 60,
                backgroundColor: 'pink',
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                top: -135,
                left: 30,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 40,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  left: 1.5,
                  top: 1,
                }}>
                30rb
              </Text>
            </View>
          </TouchableOpacity>
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                onPress={() => navigation.navigate('FormulirPemesanan', '30rb')}
                style={{
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: 'blue',
                  left: 260,
                  top: -150,
                }}>
                Pakai Kupon
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <View
              style={{
                width: 350,
                height: 150,
                marginHorizontal: 5,
                top: -120,
                borderRadius: 9,
                justifyContent: 'space-between',
                alignItems: 'center',
                left: 15,
                flexDirection: 'row',
                padding: 19,
                elevation: 3,
                backgroundColor: 'white',
              }}>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  fontWeight: 'bold',
                  textAlign: 'auto',
                  fontSize: 15,
                  left: 140,
                  color: 'black',
                  top: 30,
                  lineHeight: 17,
                }}>
                Promo Discount Cuci Sepatu 50 %
              </Text>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  textAlign: 'justify',
                  fontSize: 15,
                  left: 140,
                  color: 'gray',
                  top: 80,
                  lineHeight: 17,
                }}>
                09 s/d 15 Maret 2021
              </Text>
            </View>
            <View
              style={{
                width: 120,
                height: 120,
                borderRadius: 60,
                backgroundColor: 'pink',
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                top: -255,
                left: 30,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 40,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  left: 1.5,
                  top: -1,
                }}>
                50%
              </Text>
            </View>
          </TouchableOpacity>
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                onPress={() => navigation.navigate('FormulirPemesanan')}
                style={{
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: 'blue',
                  left: 260,
                  top: -270,
                }}>
                Pakai Kupon
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <View
              style={{
                width: 350,
                height: 150,
                marginHorizontal: 5,
                top: -241,
                borderRadius: 9,
                justifyContent: 'space-between',
                alignItems: 'center',
                left: 15,
                flexDirection: 'row',
                padding: 19,
                elevation: 3,
                backgroundColor: 'white',
              }}>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  fontWeight: 'bold',
                  textAlign: 'auto',
                  fontSize: 15,
                  left: 140,
                  color: 'black',
                  top: 30,
                  lineHeight: 17,
                }}>
                Promo Discount 50%
              </Text>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  textAlign: 'justify',
                  fontSize: 15,
                  left: 140,
                  color: 'gray',
                  top: 60,
                  lineHeight: 17,
                }}>
                09 s/d 15 Maret 2021
              </Text>
            </View>
            <View
              style={{
                width: 120,
                height: 120,
                borderRadius: 60,
                backgroundColor: 'pink',
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                top: -375,
                left: 30,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 40,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  left: 1.5,
                  top: 1,
                }}>
                50%
              </Text>
            </View>
          </TouchableOpacity>
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                onPress={() => navigation.navigate('FormulirPemesanan')}
                style={{
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: 'blue',
                  left: 260,
                  top: -390,
                }}>
                Pakai Kupon
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <View
              style={{
                width: 350,
                height: 150,
                marginHorizontal: 5,
                top: -360,
                borderRadius: 9,
                justifyContent: 'space-between',
                alignItems: 'center',
                left: 15,
                flexDirection: 'row',
                padding: 19,
                elevation: 3,
                backgroundColor: 'white',
              }}>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  fontWeight: 'bold',
                  textAlign: 'auto',
                  fontSize: 15,
                  left: 140,
                  color: 'black',
                  top: 30,
                  lineHeight: 17,
                }}>
                Promo Discount 50%
              </Text>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  textAlign: 'justify',
                  fontSize: 15,
                  left: 140,
                  color: 'gray',
                  top: 60,
                  lineHeight: 17,
                }}>
                09 s/d 15 Maret 2021
              </Text>
            </View>
            <View
              style={{
                width: 120,
                height: 120,
                borderRadius: 60,
                backgroundColor: 'pink',
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                top: -495,
                left: 30,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 40,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  left: 1.5,
                  top: -1,
                }}>
                50%
              </Text>
            </View>
          </TouchableOpacity>
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                onPress={() => navigation.navigate('FormulirPemesanan')}
                style={{
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: 'blue',
                  left: 260,
                  top: -510,
                }}>
                Pakai Kupon
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <View
              style={{
                width: 350,
                height: 150,
                marginHorizontal: 5,
                top: -480,
                borderRadius: 9,
                justifyContent: 'space-between',
                alignItems: 'center',
                left: 15,
                flexDirection: 'row',
                padding: 19,
                elevation: 3,
                backgroundColor: 'white',
              }}>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  fontWeight: 'bold',
                  textAlign: 'auto',
                  fontSize: 15,
                  left: 140,
                  color: 'black',
                  top: 30,
                  lineHeight: 17,
                }}>
                Promo Discount 50%
              </Text>
              <Text
                style={{
                  position: 'absolute',
                  width: 185,
                  height: 40,
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  textAlign: 'justify',
                  fontSize: 15,
                  left: 140,
                  color: 'gray',
                  top: 60,
                  lineHeight: 17,
                }}>
                09 s/d 15 Maret 2021
              </Text>
            </View>
            <View
              style={{
                width: 120,
                height: 120,
                borderRadius: 60,
                backgroundColor: 'pink',
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                top: -615,
                left: 30,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 40,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  left: 1.5,
                  top: 1,
                }}>
                50%
              </Text>
            </View>
          </TouchableOpacity>
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                onPress={() => navigation.navigate('FormulirPemesanan')}
                style={{
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: 'blue',
                  left: 260,
                  top: -630,
                }}>
                Pakai Kupon
              </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </ScrollView>
    </View>
  );
};

export default KodePromo;
