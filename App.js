import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SpaceScreen from './SpaceScreen';
import Login from './Login';
import AuthNavigation from './AuthNavigation';
import Register from './Register';
import ForgotPass from './ForgotPass';
import Home from './Home';
import Profil from './Profil';
import MyTabBar from './BotNav';
import Transaksi from './Transaksi';
import Edit from './Edit';
import FAQ from './FAQ';
import Detail from './Detail';
import FormulirPemesanan from './FormulirPemesanan';
import KodePromo from './KodePromo';
import Keranjang from './Keranjang';
import Summary from './Summary';
import Berhasil from './Berhasil';
import Transaksi2 from './Transaksi2';
import KodePromo2 from './KodePromo2';
import Checkout from './Checkout';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="SpaceScreen" component={SpaceScreen} />
        <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="ForgotPass" component={ForgotPass} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Profil" component={Profil} />
        <Stack.Screen name="BotNav" component={MyTabBar} />
        <Stack.Screen name="Transaksi" component={Transaksi} />
        <Stack.Screen name="FAQ" component={FAQ} />
        <Stack.Screen name="Detail" component={Detail} />
        <Stack.Screen name="FormulirPemesanan" component={FormulirPemesanan} />
        <Stack.Screen name="KodePromo" component={KodePromo} />
        <Stack.Screen
          name="Edit"
          component={Edit}
          screenOptions={{headerShown: true}}
        />
        <Stack.Screen name="Keranjang" component={Keranjang} />
        <Stack.Screen name="Summary" component={Summary} />
        <Stack.Screen name="Berhasil" component={Berhasil} />
        <Stack.Screen name="Transaksi2" component={Transaksi2} />
        <Stack.Screen name="KodePromo2" component={KodePromo2} />
        <Stack.Screen name="Checkout" component={Checkout} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
