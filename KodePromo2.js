import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';

const KodePromo2 = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 8,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={20} />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 17,
            color: '#034262',
            fontWeight: 'bold',
          }}>
          Kode Promo
        </Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: '100%',
            height: 170,
            backgroundColor: '#fff',
            paddingTop: 38,
            top: 5,
          }}>
          <Text
            style={{
              position: 'absolute',
              width: 185,
              height: 40,
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: 'bold',
              textAlign: 'auto',
              fontSize: 15,
              left: 155,
              color: 'black',
              top: 50,
              lineHeight: 17,
            }}>
            Promo Cashback Hingga 30rb
          </Text>
        </View>
        <View
          style={{
            width: 120,
            height: 120,
            borderRadius: 60,
            backgroundColor: 'pink',
            flexDirection: 'column',
            justifyContent: 'space-around',
            alignItems: 'center',
            top: -150,
            left: 10,
          }}>
          <Text
            style={{
              color: 'black',
              fontSize: 40,
              fontWeight: 'bold',
              textAlign: 'center',
              left: 1.5,
              top: -1,
            }}>
            30rb
          </Text>
        </View>
        <View
          style={{
            width: 400,
            height: 510,
            backgroundColor: '#fff',
            paddingTop: 38,
            top: -70,
          }}>
          <Text
            style={{
              color: 'black',
              fontSize: 20,
              fontWeight: 'bold',
              textAlign: 'center',
              left: -90,
              top: -1,
            }}>
            Ketentuan Promo
          </Text>
        </View>
        <View>
          <Text
            style={{
              color: 'black',
              fontSize: 19,
              textAlign: 'justify',
              alignItems: 'center',
              left: 35,
              top: -500,
            }}>
            Minimal Belanja 100rb
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('FormulirPemesanan')}
          style={{
            width: '80%',
            marginTop: -40,
            backgroundColor: '#BB2427',
            borderRadius: 10,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
            left: 38,
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 20,
              fontWeight: 'bold',
            }}>
            Gunakan Promo
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default KodePromo2;
