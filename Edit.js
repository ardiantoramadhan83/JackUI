import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';

const Edit = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <View
            style={{
              flexDirection: 'row',
              padding: 19,
              elevation: 3,
              backgroundColor: 'white',
            }}>
            <TouchableOpacity
              style={{paddingRight: 14}}
              onPress={() => navigation.goBack()}>
              <Icon name="arrowleft" size={20} />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: 'Montserrat',
                fontSize: 17,
                color: '#034262',
                fontWeight: 'bold',
              }}>
              Edit Profil
            </Text>
          </View>
          <Image
            source={require('./assets/Profil.png')}
            style={{
              width: 100,
              height: 100,
              alignSelf: 'center',
              marginTop: 30,
            }}
          />
          <TouchableOpacity>
            <Image
              source={require('./assets/pencil.png')}
              style={{
                width: 20,
                height: 20,
                left: 150,
                marginTop: 32,
                top: 3,
              }}
            />
            <Text
              style={{
                fontFamily: 'Montserrat',
                fontSize: 17,
                color: 'blue',
                textAlign: 'center',
                top: -20,
                left: 20,
              }}>
              Edit Foto
            </Text>
          </TouchableOpacity>
          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              paddingHorizontal: 20,
              paddingTop: 38,
              marginTop: -20,
            }}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Nama</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Masukan Nama" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 10}}>
              Email
            </Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Masukkan Email" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 10}}>
              No HP
            </Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Masukkan No HP" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="numeric"
            />
            <TouchableOpacity
              onPress={() => navigation.navigate('Profil')}
              style={{
                width: '100%',
                marginTop: 50,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Simpan
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default Edit;
