import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Profil = ({navigation, route}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#f6f8ff',
      }}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: Dimensions.get('screen').width,
            height: 290,
            backgroundColor: '#ffffff',
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 30,
              justifyContent: 'space-between',
              paddingLeft: 22,
              paddingRight: 34,
            }}></View>
          <Image
            source={require('./assets/Profil.png')}
            style={{
              width: 100,
              height: 100,
              alignSelf: 'center',
            }}
          />
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 25,
              color: '#034262',
              textAlign: 'center',
              fontWeight: 'bold',
              marginTop: 10,
            }}>
            Ardianto Ramadhan
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 14,
              color: '#034262',
              textAlign: 'center',
              marginTop: 5,
            }}>
            Admin@gmail.com
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Edit')}>
            <Image
              source={require('./assets/Edit.png')}
              style={{
                width: 100,
                height: 45,
                alignSelf: 'center',
                marginTop: 23,
              }}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: 350,
            height: 250,
            backgroundColor: '#fff',
            alignSelf: 'center',
            marginTop: 30,
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              fontWeight: 'bold',
              marginTop: 20,
              left: 50,
            }}>
            About
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              fontWeight: 'bold',
              marginTop: 25,
              left: 50,
            }}>
            Terms & Condition
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('FAQ')}>
            <Text
              style={{
                fontFamily: 'Montserrat',
                fontSize: 17,
                color: '#034262',
                fontWeight: 'bold',
                marginTop: 25,
                left: 50,
              }}>
              FAQ
            </Text>
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              fontWeight: 'bold',
              marginTop: 25,
              left: 50,
            }}>
            History
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 17,
              color: '#034262',
              fontWeight: 'bold',
              marginTop: 25,
              left: 50,
            }}>
            Setting
          </Text>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => navigation.navigate('Login')}
            style={{
              width: '60%',
              height: 50,
              marginTop: 30,
              backgroundColor: '#fff',
              borderRadius: 2,
              paddingVertical: 15,
              left: 85,
            }}>
            <Image
              source={require('./assets/Out.png')}
              style={{
                width: 20,
                height: 20,
                left: 10,
              }}></Image>
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                fontWeight: 'bold',
                left: 100,
                margin: -20,
              }}>
              Log Out
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Profil;
